package com.volskyioleh.testproject.ui;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract MainMapFragment provideListFragment();

    @ContributesAndroidInjector
    abstract NotificationsListFragment provideDetailsFragment();
}
