package com.volskyioleh.testproject.ui;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.volskyioleh.testproject.R;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationView = findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (item -> {
                    Fragment selectedFragment = null;
                    switch (item.getItemId()) {
                        case R.id.navigation_1:
                            selectedFragment = MainMapFragment.newInstance();
                            break;
                        case R.id.navigation_2:
                            selectedFragment = NotificationsListFragment.newInstance(true);
                            break;
                        case R.id.navigation_3:
                            selectedFragment = NotificationsListFragment.newInstance(false);
                            break;
                    }
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frgmCont, Objects.requireNonNull(selectedFragment));
                    transaction.commit();
                    return true;
                });


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frgmCont, MainMapFragment.newInstance());
        transaction.commit();

    }


}
