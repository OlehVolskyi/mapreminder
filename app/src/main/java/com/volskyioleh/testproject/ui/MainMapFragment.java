package com.volskyioleh.testproject.ui;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.volskyioleh.testproject.MyAlarmReceiver;
import com.volskyioleh.testproject.R;
import com.volskyioleh.testproject.data.TImeModel;
import com.volskyioleh.testproject.data.db.NotificationModel;
import com.volskyioleh.testproject.data.rest.GetIpInfoApi;
import com.volskyioleh.testproject.data.rest.IpInfoModel;
import com.volskyioleh.testproject.data.rest.RetrofitClient;
import com.volskyioleh.testproject.vm.AddNewNotificationViewModel;
import com.volskyioleh.testproject.vm.AllNotificationListViewModel;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainMapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "MapActivity";
    private GetIpInfoApi api = RetrofitClient.getInstance().getApi();

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1224;
    private static final float DEFAULT_ZOOM = 10f;

    private Boolean mLocationPermissionsGranted = false;
    private TImeModel tImeModel = new TImeModel();
    private MapView mMapView;
    private GoogleMap mGoogleMap;


    public static MainMapFragment newInstance() {
        MainMapFragment fragment = new MainMapFragment();
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_map, container, false);

        getLocationPermission();

        mMapView = rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        try {
            MapsInitializer.initialize(Objects.requireNonNull(getContext()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        return rootView;
    }

    private void getLocationPermission() {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()).getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
            } else {
                requestPermissions(permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            requestPermissions(permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int grantResult : grantResults) {
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    showCurrentLocation();
                    getDeviceLocation();
                }
            }
        }
    }

    private void showCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }


    private void moveCamera(LatLng latLng, float zoom, String title) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        if (!title.equals("My Location")) {
            MarkerOptions options = new MarkerOptions()
                    .position(latLng)
                    .title(title);
            mGoogleMap.addMarker(options);
        }
    }


    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: getting the devices current location");

        FusedLocationProviderClient mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(Objects.requireNonNull(getContext()));

        try {
            if (mLocationPermissionsGranted) {

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onComplete: found location!");
                        Location currentLocation = (Location) task.getResult();

                        if (currentLocation != null) {
                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                    DEFAULT_ZOOM, getString(R.string.my_location));
                        }

                    } else {
                        Toast.makeText(getActivity(), "unable to get current location", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    public void getIpInfo() {
        api.getIPInfo()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<IpInfoModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                    }

                    @Override
                    public void onNext(IpInfoModel ipInfoModel) {
                        moveCamera(new LatLng(ipInfoModel.getCity().getLat(), ipInfoModel.getCity().getLon()), 10F, "My Location");
                        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(ipInfoModel.getCity().getLat(), ipInfoModel.getCity().getLon())).title("Your Location"));
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.d(TAG, "OnError");
                        t.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }


    private void setDatePicker(final EditText textView) {

        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        final int month = c.get(Calendar.MONTH);
        final int day = c.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(Objects.requireNonNull(getContext()), (view, year1, month1, dayOfMonth) -> {

            String date = String.valueOf(dayOfMonth) + "/" + String.valueOf(month1 + 1)
                    + "/" + String.valueOf(year1);
            textView.setText(date);
            tImeModel.setmDay(dayOfMonth);
            tImeModel.setmMonth(month1);
            tImeModel.setmYear(year1);
        }, year, month, day).show();
    }


    private void setNotification(NotificationModel model) {
        Intent intent = new Intent(getContext(), MyAlarmReceiver.class);
        int RQS_1 = (int) System.currentTimeMillis();
        intent.putExtra(MyAlarmReceiver.TITLE, model.getTitle());
        intent.putExtra(MyAlarmReceiver.DATE, convertMillisToDate(model.getDate()));
        intent.putExtra(MyAlarmReceiver.ID, model.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getContext(), RQS_1, intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmManager = (AlarmManager) Objects.requireNonNull(getContext()).getSystemService(Context.ALARM_SERVICE);

        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, model.getReminder_date(), pendingIntent);
        }

    }

    private void setTimePicker(EditText timeEditText) {
        Calendar c = Calendar.getInstance();

        final int hours = c.get(Calendar.HOUR);
        final int minutes = c.get(Calendar.MINUTE);
        new TimePickerDialog(getContext(), (view, hourOfDay, minute) -> {
            String time = String.valueOf(hourOfDay) + ":" + String.valueOf(minute);
            tImeModel.setmHour(hourOfDay);
            tImeModel.setmMinute(minute);
            timeEditText.setText(time);
        }, hours, minutes, true).show();

    }


    @SuppressLint("ClickableViewAccessibility")
    public void onCreateDialog(final LatLng latLng) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.create_marker_layout, null);

        final EditText dateEditText = dialogLayout.findViewById(R.id.dateEdtx);
        dateEditText.setInputType(InputType.TYPE_NULL);
        dateEditText.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                setDatePicker(dateEditText);
                return true;
            }
            return false;
        });


        final EditText timeEditText = dialogLayout.findViewById(R.id.timeEdtxt);
        timeEditText.setInputType(InputType.TYPE_NULL);
;
        timeEditText.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                setTimePicker(timeEditText);
                return true;
            }
            return false;
        });

        final EditText title = dialogLayout.findViewById(R.id.titleEdtx);
        final EditText remind = dialogLayout.findViewById(R.id.remindEdtxt);
        final EditText addressEditText = dialogLayout.findViewById(R.id.adressEdtxt);
        final EditText descrEditText = dialogLayout.findViewById(R.id.deskEdtx);
        addressEditText.setText(getAdress(latLng));


        builder.setView(dialogLayout);
        builder.setPositiveButton("Create notification", (dialogInterface, i) -> {
            if (dateEditText.getText() != null && timeEditText.getText() != null) {

                NotificationModel notificationModel = new NotificationModel(String.valueOf(title.getText()), String.valueOf(addressEditText.getText()),
                        String.valueOf(descrEditText.getText())
                        , latLng.longitude, latLng.latitude, toMilisFromDate(false, 0)
                        , toMilisFromDate(true, Integer.parseInt(String.valueOf(remind.getText()))));

                new AddNewNotificationViewModel(Objects.requireNonNull(getActivity()).getApplication())
                        .addItem(notificationModel);
                setNotification(notificationModel);
            } else {
                Toast.makeText(getContext(), getString(R.string.date_null), Toast.LENGTH_LONG).show();
            }
        });

        builder.show();
    }


    private String convertMillisToDate(long dateInMillis) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy  HH:mm");
        return formatter.format(new Date(dateInMillis));
    }


    private long toMilisFromDate(boolean isRemind, int remindHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(tImeModel.getmYear(), tImeModel.getmMonth(), tImeModel.getmDay(), tImeModel.getmHour(), tImeModel.getmMinute());
        Log.d(TAG, calendar.getTimeInMillis() + " " + tImeModel.getmYear() + " " + tImeModel.getmMonth() + " " +
                tImeModel.getmDay() + " " + tImeModel.getmHour() + " " + tImeModel.getmMinute());
        if (isRemind) {
            calendar.add(Calendar.HOUR, -remindHours);
        }


        return calendar.getTimeInMillis();
    }


    private String getAdress(LatLng latLng) {
        Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
        String address = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); //1 num of possible location returned
            address = addresses.get(0).getAddressLine(0); //0 to obtain first possible address

            Log.d(TAG, address);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (mLocationPermissionsGranted) {
            getDeviceLocation();
            showCurrentLocation();
        } else {
            getIpInfo();
        }

        AllNotificationListViewModel mItemViewModel = ViewModelProviders.of(this).get(AllNotificationListViewModel.class);
        mItemViewModel.getNotificationsList().observe(Objects.requireNonNull(getActivity()), itemModels -> {
            if (itemModels != null && !itemModels.isEmpty()) {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (int i = 0; i < itemModels.size(); i++) {
                    MarkerOptions options = new MarkerOptions()
                            .position(new LatLng(itemModels.get(i).getLat(), itemModels.get(i).getLon()))
                            .title(itemModels.get(i).getTitle());
                    mGoogleMap.addMarker(options);
                    builder.include(new LatLng(itemModels.get(i).getLat(), itemModels.get(i).getLon()));
                }
                LatLngBounds bounds = builder.build();
                setCameraZoom(bounds);
            }
        });

        mGoogleMap.setOnMapClickListener(this::onCreateDialog);
    }

    private void setCameraZoom(LatLngBounds bounds) {
        int padding = 100;
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mGoogleMap.moveCamera(cu);
    }

}
