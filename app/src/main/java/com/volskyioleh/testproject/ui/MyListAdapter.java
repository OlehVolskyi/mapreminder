package com.volskyioleh.testproject.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.volskyioleh.testproject.R;
import com.volskyioleh.testproject.data.db.NotificationModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ItemViewHolder> {
    private List<NotificationModel> mList;
    private Context mContext;

    MyListAdapter(List<NotificationModel> itemsList, Context context) {
        mList = itemsList;
        mContext = context;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        NotificationModel model = mList.get(position);
        holder.mAddress.setText(model.getAddress());
        holder.mDate.setText(convertMillisToDate(model.getDate()));
        holder.mDescriprion.setText(model.getDescription());
        holder.mTitle.setText(model.getTitle());
    }

    private String convertMillisToDate(long dateInMillis) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy  HH:mm");
        return formatter.format(new Date(dateInMillis));
    }



    public void addItem(List<NotificationModel> itemModels) {
        mList = itemModels;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTv)
        TextView mTitle;
        @BindView(R.id.addressTv)
        TextView mAddress;
        @BindView(R.id.dateTv)
        TextView mDate;
        @BindView(R.id.desrcTv)
        TextView mDescriprion;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

