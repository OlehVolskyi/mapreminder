package com.volskyioleh.testproject.ui;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.volskyioleh.testproject.R;
import com.volskyioleh.testproject.vm.ListViewModel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsListFragment extends Fragment {
    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;
    private boolean mIsActual;
    private MyListAdapter mAdapter;

    public static NotificationsListFragment newInstance(boolean isActual) {
        NotificationsListFragment fragment = new NotificationsListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_ACTUAL", isActual);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_notifications, container, false);
        ButterKnife.bind(this, view);

        mIsActual = getArguments().getBoolean("IS_ACTUAL");
        mAdapter = new MyListAdapter(new ArrayList<>(), getContext());
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        ListViewModel mItemViewModel = ViewModelProviders.of(this).get(ListViewModel.class);

        if (mIsActual) {
            mItemViewModel.getNotificationsList(getCurrentDate()).observe(Objects.requireNonNull(getActivity()), itemModels -> mAdapter.addItem(itemModels));
        } else {
            mItemViewModel.getHistoryNotificationsList(getCurrentDate()).observe(Objects.requireNonNull(getActivity()), itemModels -> mAdapter.addItem(itemModels));
        }
        return view;
    }

    private long getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTimeInMillis();
    }

}
