package com.volskyioleh.testproject.data.db;


import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface NotificationsDao {


    @Query("SELECT id,title,address,date,eminder_date,description, lat, lon FROM notificationmodel WHERE date > :currentTime  ORDER BY date ASC")
    LiveData<List<NotificationModel>> getActualNotifications(long currentTime);

    @Query("SELECT id,title,address,date,eminder_date,description, lat, lon FROM notificationmodel WHERE date < :currentTime ORDER BY date DESC")
    LiveData<List<NotificationModel>> getHistoryNotification(long currentTime);

    @Query("SELECT id,title,address,date,eminder_date,description, lat, lon FROM notificationmodel ORDER BY date")
    LiveData<List<NotificationModel>> getAllItems();

    @Insert(onConflict = REPLACE)
    void insertAll(NotificationModel notificationModel);

}
