package com.volskyioleh.testproject.data.rest;

public class IpInfoModel {

 private CityModel city;

    public IpInfoModel(CityModel city) {
        this.city = city;

    }

    public CityModel getCity() {
        return city;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }
}
