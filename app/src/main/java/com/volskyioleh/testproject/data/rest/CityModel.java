package com.volskyioleh.testproject.data.rest;

public class CityModel {

    private int id;
    private double lat;
    private double lon;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public CityModel( int id, long lat, long lon) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
    }
}
