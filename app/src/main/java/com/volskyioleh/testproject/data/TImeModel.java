package com.volskyioleh.testproject.data;

public class TImeModel {
    private int mYear;
    private int mMonth;
    private int mDay;
    private int mHour;

    public int getmYear() {
        return mYear;
    }

    public void setmYear(int mYear) {
        this.mYear = mYear;
    }

    public int getmMonth() {
        return mMonth;
    }

    public void setmMonth(int mMonth) {
        this.mMonth = mMonth;
    }

    public int getmDay() {
        return mDay;
    }

    public void setmDay(int mDay) {
        this.mDay = mDay;
    }

    public int getmHour() {
        return mHour;
    }

    public void setmHour(int mHour) {
        this.mHour = mHour;
    }

    public int getmMinute() {
        return mMinute;
    }

    public void setmMinute(int mMinute) {
        this.mMinute = mMinute;
    }

    int mMinute;

    public TImeModel(){

    }

    public TImeModel(int mHour, int mMinute) {
        this.mHour = mHour;
        this.mMinute = mMinute;
    }

    public TImeModel(int mYear, int mMonth, int mDay, int mHour, int mMinute) {
        this.mYear = mYear;
        this.mMonth = mMonth;
        this.mDay = mDay;
        this.mHour = mHour;
        this.mMinute = mMinute;
    }

    public TImeModel(int mYear, int mMonth, int mDay) {
        this.mYear = mYear;
        this.mMonth = mMonth;
        this.mDay = mDay;
    }
}
