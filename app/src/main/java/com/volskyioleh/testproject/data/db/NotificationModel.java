package com.volskyioleh.testproject.data.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class NotificationModel {
    @ColumnInfo(name = "title")
    private String title;

    @ColumnInfo(name = "address")
    private String address;

    @ColumnInfo(name = "description")

    private String description;

    @ColumnInfo(name = "lon")
    private double lon;

    @ColumnInfo(name = "lat")
    private double lat;

    @ColumnInfo(name = "date")
    private long date;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "eminder_date")
    private long reminder_date;



    public NotificationModel(String title, String address, String description, double lon, double lat, long date, long reminder_date) {
        this.title = title;
        this.address = address;
        this.description = description;
        this.lon = lon;
        this.lat = lat;
        this.date = date;
        this.reminder_date = reminder_date;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getReminder_date() {
        return reminder_date;
    }

    public void setReminder_date(long reminder_date) {
        this.reminder_date = reminder_date;
    }

}
