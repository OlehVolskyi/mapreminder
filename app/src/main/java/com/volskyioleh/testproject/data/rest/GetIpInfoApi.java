package com.volskyioleh.testproject.data.rest;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface GetIpInfoApi {
    @GET("json")
    Observable<IpInfoModel> getIPInfo();
}
