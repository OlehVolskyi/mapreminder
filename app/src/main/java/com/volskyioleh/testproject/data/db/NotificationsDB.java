package com.volskyioleh.testproject.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = NotificationModel.class, version = 1)
public abstract class NotificationsDB extends RoomDatabase {

    private static NotificationsDB mInstance;

    public static NotificationsDB getDatabase(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context.getApplicationContext(), NotificationsDB.class, "notifications_db")
                    .build();
        }
        return mInstance;
    }

    public static void destroyInstances() {
        mInstance = null;
    }

    public abstract NotificationsDao notificationsModelDao();

}
