package com.volskyioleh.testproject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.TaskStackBuilder;

import com.volskyioleh.testproject.ui.MainActivity;

import java.util.Objects;

import static android.support.v4.app.NotificationManagerCompat.IMPORTANCE_DEFAULT;

public class MyAlarmReceiver extends BroadcastReceiver {
    public static final String ANDROID_CHANNEL_ID = "com.example.volskyioleh.testApp.ANDROID";
    public static final String TITLE = "title";
    public static final String DATE = "date";
    public static String ID = "id";

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        String title = Objects.requireNonNull(intent.getExtras()).getString(TITLE);
        String date = intent.getExtras().getString(DATE);
        int id = intent.getIntExtra(ID, 0);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(id, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Builder builder = new Notification.Builder(context);

        Notification notification = builder.setContentTitle(title)
                .setContentText(date)
                .setAutoCancel(false)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent).build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(ANDROID_CHANNEL_ID);
        }

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel channel = new NotificationChannel(
                    ANDROID_CHANNEL_ID,
                    "NotificationDemo",
                    IMPORTANCE_DEFAULT
            );
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            notificationManager.notify(id,notification);
        }
    }

}
