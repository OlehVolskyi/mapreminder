package com.volskyioleh.testproject.vm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

import com.volskyioleh.testproject.data.db.NotificationModel;
import com.volskyioleh.testproject.data.db.NotificationsDB;

import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;

public class AddNewNotificationViewModel extends AndroidViewModel {

    private NotificationsDB mNotificationsDB;

    public AddNewNotificationViewModel(@NonNull Application application) {
        super(application);

        mNotificationsDB = NotificationsDB.getDatabase(getApplication());
    }

    public void addItem(final NotificationModel newItem) {
        Completable.fromAction(() -> mNotificationsDB.notificationsModelDao().insertAll(newItem))
                .subscribeOn(Schedulers.io())
                .subscribe();
    }

}
