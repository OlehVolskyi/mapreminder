package com.volskyioleh.testproject.vm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.volskyioleh.testproject.data.db.NotificationModel;
import com.volskyioleh.testproject.data.db.NotificationsDB;

import java.util.List;

public class AllNotificationListViewModel extends AndroidViewModel {

    private final LiveData<List<NotificationModel>> mList;

    public AllNotificationListViewModel(@NonNull Application application) {
        super(application);
        NotificationsDB mAppDatabase = NotificationsDB.getDatabase(this.getApplication());
        mList = mAppDatabase.notificationsModelDao().getAllItems();
    }

    public LiveData<List<NotificationModel>> getNotificationsList() {
        return mList;
    }
}
