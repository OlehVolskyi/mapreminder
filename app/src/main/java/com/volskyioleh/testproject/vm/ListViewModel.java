package com.volskyioleh.testproject.vm;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.volskyioleh.testproject.data.db.NotificationModel;
import com.volskyioleh.testproject.data.db.NotificationsDB;

import java.util.List;

public class ListViewModel extends AndroidViewModel {

    private final NotificationsDB mAppDatabase;

    public ListViewModel(@NonNull Application application) {
        super(application);
        mAppDatabase = NotificationsDB.getDatabase(this.getApplication());
    }

    public LiveData<List<NotificationModel>> getNotificationsList(long currentDate) {
        return mAppDatabase.notificationsModelDao().getActualNotifications(currentDate);
    }

    public LiveData<List<NotificationModel>> getHistoryNotificationsList(long currentDate) {
        return mAppDatabase.notificationsModelDao().getHistoryNotification(currentDate);
    }
}
